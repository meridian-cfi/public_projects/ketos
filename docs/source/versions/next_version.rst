Next version
============

We plan to include the following changes to future versions:  
  
**Version 3.0.0**
    * Add Neural Architecture Search capabilities, which would allow it to recommend the optimal architecture to be used in a given task.
    * Add high-level transfer learning functionalities, which will make it simple to to adapt pre-trained models to new environments and tasks.
    * Add methods to calibrate neural networks so that the output scores correspond more closely to probabilities/confidences.



ResNet
======

.. automodule:: ketos.neural_networks.resnet
   :members:
   :undoc-members:
   :show-inheritance:
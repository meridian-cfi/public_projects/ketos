.. _selection_table:

Selection Table
================

.. automodule:: ketos.data_handling.selection_table
   :members:
   :undoc-members:
   :show-inheritance:
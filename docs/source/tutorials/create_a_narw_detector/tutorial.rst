Tutorial: Creating a detector
=============================

.. note::  You can download materials to follow along this tutorial from the links below:
* :download:`Executable version (jupyter notebook) <https://gitlab.meridian.cs.dal.ca/public_projects/ketos_tutorials/-/blob/master/tutorials/train_a_narw_classifier/create_a_narw_detector.ipynb>`   
* :download:`The pre-trained narw model <https://gitlab.meridian.cs.dal.ca/public_projects/ketos_tutorials/-/blob/master/tutorials/create_a_narw_detector/narw.kt>`   
* :download:`Sample data <https://gitlab.meridian.cs.dal.ca/public_projects/ketos_tutorials/-/blob/master/tutorials/create_a_narw_detector/data.zip>`   




.. raw:: html
   :file: static/create_a_narw_detector.html

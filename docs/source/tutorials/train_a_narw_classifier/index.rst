Tutorial: Training a Binary ResNet Classifier
=============================================

 .. note:: You can download an executable version (Jupyter Notebook) of this tutorial and the data required to follow along :download:`here <https://gitlab.meridian.cs.dal.ca/public_projects/ketos_tutorials/-/blob/master/tutorials/train_a_narw_classifier/train_a_narw_detector_part_1.zip>`.

.. raw:: html
   :file: static/train_a_narw_detector_part_1.html

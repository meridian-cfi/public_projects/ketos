Creating a database 
===================

In this tutorial we will use Ketos to build a database of North Atlantic Right Whale upcalls.
The audio will be represented as spectrograms and can later be used to train a deep learning based classfier and build a right whale detector.


 .. note:: You can download an executable version (Jupyter Notebook) of this tutorial, the data needed to follow along :download:`here <https://gitlab.meridian.cs.dal.ca/public_projects/ketos_tutorials/-/blob/master/tutorials/create_database/database_creation_tutorial.zip>`.

.. raw:: html
   :file: static/database_creation.html
